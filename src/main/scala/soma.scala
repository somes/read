package hu.lezsak.domonkos.pubmed_some

import ujson.Obj

import scala.annotation.targetName

def extractSomas(s: String): Iterator[String] = {
  val candidates = raw"\w+".r
    .findAllIn(s)
    .map(_.toLowerCase)

  val interestingWords =
    candidates.filter(_.endsWith("some")) ++
    candidates.filter(_.endsWith("somes")).map(_.dropRight(1))

  interestingWords.filterNot(words)
}

case class SomaOrigin(pmid: Int, issued: Option[Date], authors: Set[String]) {
  def toUJsonObj: ujson.Obj = ujson.Obj(
    "pmid" -> pmid,
    "issued" -> {
      issued match
        case None => ujson.Null
        case Some(date) =>
          val d: java.time.LocalDate = date.convert
          ujson.Str(d.toString)
    },
    "authors" -> authors,
  )
}

case object SomaOrigin {
  def fromPubmedArticle(article: xml.Node): Option[SomaOrigin] =
    for
      pmid <- (article \ "MedlineCitation" \ "PMID").headOption flatMap (_.text.toIntOption)
      issued = (article \ "MedlineCitation" \ "DateCompleted").headOption flatMap Date.from
      authors = (article \ "MedlineCitation" \ "Article" \ "AuthorList" \ "Author") flatMap extractAuthorName
    yield SomaOrigin(pmid, issued, authors.toSet)

  given upickle.default.Writer[SomaOrigin] =
    upickle.default.writer[ujson.Value].comap(_.toUJsonObj)

  given Ordering[SomaOrigin] with {
    def compare(a: SomaOrigin, b: SomaOrigin): Int =
      a.pmid.compareTo(b.pmid)
  }
}

case class SomaInfo(origin: SomaOrigin, mentions: Int = 1) {
  @targetName("add")
  def +(that: SomaInfo): SomaInfo =
    SomaInfo(min(this.origin, that.origin), this.mentions + that.mentions)

  def toUJsonObj: ujson.Obj = ujson.Obj(
    "mentions" -> mentions,
    origin.toUJsonObj.value.toSeq *
  )
}

case object SomaInfo {
  given upickle.default.Writer[SomaInfo] = upickle.default.writer[ujson.Value].comap(_.toUJsonObj)
}
