package hu.lezsak.domonkos.pubmed_some

import java.io.{File, FileInputStream, FileWriter}
import java.time.{Duration, Instant}
import java.util.concurrent.{ArrayBlockingQueue, ForkJoinPool, ThreadPoolExecutor}
import java.util.concurrent.atomic.AtomicInteger
import java.util.zip.GZIPInputStream
import scala.annotation.targetName
import scala.collection.parallel.CollectionConverters.*
import scala.collection.parallel.{ExecutionContextTaskSupport, ParSeq}
import scala.concurrent.{ExecutionContext, Future}
import scala.util
import scala.util.{Failure, Success, Try, Using}


def wrap[A, B, C, D](beforeHook: A => C, afterHook: (A, B, C) => D)(f: A => B): A => D = {
  a => {
    val c = beforeHook(a)
    val b = f(a)
    afterHook(a, b, c)
  }
}

def processFile(filename: String): Try[Map[String, SomaInfo]] = {
  Using(new FileInputStream(filename)) {
    fileInputStream =>
      Using(new GZIPInputStream(fileInputStream)) {
        gzipInputStream => {
          for
            (text, node) <- extractArticles(gzipInputStream)
          yield {
            for
              word <- extractSomas(text)
              origin <- SomaOrigin.fromPubmedArticle(node)
            yield word -> SomaInfo(origin)
          }.toMap
        }.folded(_ + _)
      }
  }.flatten
}

case class Result(somas: Map[String, SomaInfo] = Map(), failed: Set[String] = Set()) {
  @targetName("plus")
  def +(that: Result): Result =
    Result(Seq(this.somas, that.somas).folded(_ + _))
}

case object Result {
  def empty: Result = new Result()

  def apply(file: String, result: Try[Map[String, SomaInfo]]): Result = result match
    case Success(somas) => Result(somas = somas)
    case Failure(_) => Result(failed = Set(file))

  def apply(t: (String, Try[Map[String, SomaInfo]])): Result =
    apply(t._1, t._2)
}

@main
def main(): Unit = {
  val folder: File = new File("/home/led/Downloads/pubmed/ftp.ncbi.nlm.nih.gov/pubmed/baseline")
  val files = folder.listFiles
    .filter(_.getName.endsWith(".xml.gz"))
    .map(_.getPath)
    .toList.sorted

  val parallelFiles = files.par
  val executionContext = ExecutionContext fromExecutorService ThreadPoolExecutor(
    Runtime.getRuntime.availableProcessors() / 2, Runtime.getRuntime.availableProcessors(),
    1, java.util.concurrent.TimeUnit.SECONDS,
    ArrayBlockingQueue[java.lang.Runnable](2000),
  )
  parallelFiles.tasksupport = ExecutionContextTaskSupport(executionContext)

  val startedAt = Instant.now()
  val totalCount = files.length
  val processingCount = new AtomicInteger(0)
  val finishedCount = new AtomicInteger(0)

  var partialResults: Map[String, SomaInfo] = Map()
  def beforeHook(filename: String) = {
    val etaString = {
      val finishedCountNow = finishedCount.get()
      val dateTimeNow = Instant.now()

      if (finishedCountNow > 0) {
        val elapsed = Duration.between(startedAt, dateTimeNow)
        val remaining = Duration.ofMillis(elapsed.toMillis * totalCount / finishedCountNow)
        s"ETA: $remaining (${dateTimeNow.plus(remaining)})"
      } else "ETA unknown"
    }

    println(s"[${finishedCount.get()} + ${processingCount.incrementAndGet()} / $totalCount] " +
      s"> $filename; $etaString")
    Instant.now()
  }
  def afterHook(filename: String, result: Try[Map[String, SomaInfo]], startTime: Instant) = {
    val dateTimeNow = Instant.now()
    val elapsed = Duration.between(startTime, dateTimeNow)

    val msgPrefix = s"[${finishedCount.incrementAndGet()} + ${processingCount.decrementAndGet()} / $totalCount] "
    val msgPostfix = s" in $elapsed"
    result match {
      case Success(map) =>
        println(msgPrefix + s"✔ $filename: ${map.size} words" + msgPostfix)

        Future {
          Using(new FileWriter(new File(filename + ".json"))) {
            upickle.default.writeTo(map, _, indent = 2)
          }

          synchronized {
            partialResults = Seq(partialResults, map).folded(_ + _)
          }

          val msgPrefix = " ".repeat(s"[${finishedCount.get()} + ${processingCount.get()} + $totalCount] ".length)
          println(msgPrefix + partialResults.size + " " + upickle.default.write(partialResults))
        }(scala.concurrent.ExecutionContext.global)

      case Failure(exc) =>
        println(msgPrefix + s"❌ $filename: $exc" + msgPostfix)
    }

    (filename, result)
  }

  println("Program completed")
  println()

  val results = parallelFiles map {
    wrap[String, Try[Map[String, SomaInfo]], Instant, (String, Try[Map[String, SomaInfo]])]
      (beforeHook, afterHook)
      (processFile)
  } map Result
  val result = results.fold(Result.empty)(_ + _)

  println("Failed to process:")
  println(result.failed.mkString(", "))
  println()

  val outFile = folder.getPath + "/somes.json"
  Using(FileWriter(outFile)) {
    upickle.default.writeTo(result.somas, _)
  }
  println("Collected data written to " + outFile)
}