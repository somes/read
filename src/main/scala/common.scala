package hu.lezsak.domonkos.pubmed_some

import math.Ordering.Implicits.infixOrderingOps
def min[T: Ordering](a: T, b: T): T =
  if a <= b then a else b

extension[K, V] (m: Seq[Map[K, V]])
  def folded(op: (V, V) => V): Map[K, V] =
    m.fold (Map () ) { (a, b) =>
      (a.toSeq ++ b.toSeq)
        .groupMapReduce(_._1)(_._2)(op)
    }
