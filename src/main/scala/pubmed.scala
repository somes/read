package hu.lezsak.domonkos.pubmed_some

import java.io.InputStream
import javax.xml.parsers.SAXParserFactory

case class Date(year: Int, month: Int, day: Int)
case object Date {
  def from(node: xml.Node): Option[Date] =
    for
      year <- (node \ "Year").headOption flatMap (_.text.toIntOption)
      month <- (node \ "Month").headOption flatMap (_.text.toIntOption)
      day <- (node \ "Day").headOption flatMap (_.text.toIntOption)
    yield Date(year, month, day)

  given Conversion[Date, java.time.LocalDate] = date => java.time.LocalDate.of(date.year, date.month, date.day)
}

def extractArticles(is: InputStream): Seq[(String, xml.Node)] = {
  val source = xml.Source.fromInputStream(is)
  val xmlLoader = xml.XML.withSAXParser({
    val factory = SAXParserFactory.newInstance()
    factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false)
    factory.newSAXParser()
  })
  val xmlRoot = xmlLoader.load(source) // todo don't load, just stream
  val articles = xmlRoot \ "PubmedArticle"
  articles.map(n => (((n \ "MedlineCitation" \ "Article" \ "ArticleTitle") ++ (n \ "MedlineCitation" \ "Article" \ "Abstract" \ "AbstractText")).mkString("\n"), n))
}

def extractAuthorName(node: xml.Node): Option[String] =
  val parts = node \ "ForeName" ++ node \ "LastName" ++ node \ "Suffix" ++ node \ "CollectiveName"
  if parts.isEmpty then None
  else Some {
    parts.map(part => part.text)
      .mkString(" ")
  }
