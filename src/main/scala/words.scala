package hu.lezsak.domonkos.pubmed_some

import scala.io.Source

private def contentsOf(filename: String) = {
  val source = Source.fromFile(filename)
  source.getLines()
}

private val files = Seq(
  "/usr/share/dict/words",
  "words.txt",
)

val words: Set[String] = {
  Set.from(files.flatMap(contentsOf))
}
