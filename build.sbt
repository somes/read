import sbt.Keys.libraryDependencies

ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "3.3.1"

lazy val root = (project in file("."))
  .settings(
    name := "pubmed-some",
    idePackagePrefix := Some("hu.lezsak.domonkos.pubmed_some"),
    libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "2.1.0",
    libraryDependencies += "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.4",
    libraryDependencies += "org.scala-lang" %% "toolkit" % "0.2.0",
    scalacOptions += "-deprecation",
  )
